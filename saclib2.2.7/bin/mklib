#!/bin/bash

# USAGE:
#   mklib <std|deb|opt|all>
#
# FUNCTION
#   Creates ".a" files in "$saclib/lib/" depending on the argument:
#   - 'std' causes a standard library to be built. The library file will have
#     the name "saclib.a" and the corresponding object files are in
#     "saclib/lib/obj".   
#   - 'deb' switches on the '-g' option of the compiler which includes 
#     debugging information in the object files. The library file will have
#     the name "saclibd.a" and the corresponding object files are in
#     "saclib/lib/objd".  
#   - 'opt' switches on the '-O' option which produces optimized code. The
#     library file will have the name "saclibo.a" and the corresponding object
#     files are in "saclib/lib/objo".
#   - 'all' builds all three types of libraries.

# Exit with an error if there is an error on building.
set -e

if [ $# -lt 1 ] 
then
  echo "USAGE:"
  echo "  mklib <deb|opt|all>"
  exit
fi

if [ $1 = "clean" ] 
then
  ### remove .o and .a and makefiles
  echo "Removing object files, libraries and makefiles ..."
  for i in lib/objo lib/objd lib; do
   pushd >/dev/null $saclib/$i
   'rm' -f *
   popd >/dev/null
   done
  ### do sysdep cleanup
  echo "Removing system-dependent files ..."
  for i in linuxX86 linuxX86_64 solarisSparc macosX86 macosX86_64 windowsX86_64 wasm; do
   pushd >/dev/null $saclib/sysdep/$i
   ./cleanup
   popd >/dev/null
   done
  exit
fi


if [ "$CC" != "" ] 
then
  CC_OPTION="CC=$CC"
  echo "Compiling with" $CC
fi

if [ "$TOOLCHAIN" != "" ]
then
  echo "Using toolchain" $TOOLCHAIN
fi

if [ $1 = "std" ]
then
  echo "This option no longer exists!"
elif [ $1 = "deb" ]; then
  pushd >/dev/null  $saclib/lib/objd
  $TOOLCHAIN make $CC_OPTION "SACFLAG=-g -DNO_SACLIB_MACROS" EXTENSION=d
  popd >/dev/null
elif [ $1 = "opt" ]; then
  pushd >/dev/null  $saclib/lib/objo
  $TOOLCHAIN make $CC_OPTION "SACFLAG=" EXTENSION=o
  popd >/dev/null
elif [ $1 = "all" ]; then
  pushd >/dev/null  $saclib/lib/objd
  $TOOLCHAIN make $CC_OPTION "SACFLAG=-g -DNO_SACLIB_MACROS" EXTENSION=d
  popd >/dev/null
  pushd >/dev/null  $saclib/lib/objo
  $TOOLCHAIN make $CC_OPTION "SACFLAG=" EXTENSION=o
  popd >/dev/null
else
  echo "USAGE:"
  echo "  mklib <deb|opt|all>"
  exit
fi

echo "mklib done."
