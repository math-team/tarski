[
ex s,j,v [
 - beta*j*s + c*j - mu*s + mu - phi*s + theta*v = 0
 /\ beta*j*s + beta*j*sigma*v - c*j - j*mu = 0
 /\  - beta*j*sigma*v - mu*v + phi*s - theta*v = 0 /\ j > 0 /\ v > 0
 /\ s > 0
 /\
 mu > 0 /\ beta > 0 /\ sigma > 0 /\ sigma - 1 < 0 /\ c > 0 /\ phi > 0 /\ theta > 0
]]


Rewritten as:
[
theta > 0 /\ phi > 0 /\ c > 0 /\ sigma - 1 < 0 /\ sigma > 0 
/\ beta > 0 /\ mu > 0 /\ j > 0 
/\ mu^2 + j sigma beta mu + j beta mu - beta mu +
c mu + phi mu + theta mu + j^2 sigma beta^2 - j sigma beta^2 + j c
sigma beta + j phi sigma beta - phi sigma beta + j theta beta - theta
beta + phi c + theta c = 0
]



[]
(beta,mu,sigma,theta,phi,c,j)
6
(E j)[ j > 0 /\ mu^2+j sigma beta mu+j beta mu-beta mu+c mu+phi mu+theta mu+j^2 sigma beta^2-j sigma beta^2+j c sigma beta+j phi sigma beta-phi sigma beta+j theta beta-theta beta+phi c+theta c = 0].
assume [theta > 0 /\ phi > 0 /\ c > 0 /\ sigma-1 < 0 /\ sigma > 0 /\
beta > 0 /\ mu > 0]

Note:  With variable order (beta,mu,c,sigma,theta,phi,j) it goes
through no problem ... go figure!


Resolve[
Exists[j,
theta > 0 && phi > 0 && c > 0 && sigma - 1 < 0 && sigma > 0 
&& beta > 0 && mu > 0 && j > 0 
&& mu^2 + j sigma beta mu + j beta mu - beta mu +
c mu + phi mu + theta mu + j^2 sigma beta^2 - j sigma beta^2 + j c
sigma beta + j phi sigma beta - phi sigma beta + j theta beta - theta
beta + phi c + theta c == 0
]
]

Off[General::spell1]

Assuming[theta > 0 && phi > 0 && c > 0 && sigma - 1 < 0 && sigma > 0 
&& beta > 0 && mu > 0,
Resolve[
Exists[j,
j > 0 
&& mu^2 + j sigma beta mu + j beta mu - beta mu +
c mu + phi mu + theta mu + j^2 sigma beta^2 - j sigma beta^2 + j c
sigma beta + j phi sigma beta - phi sigma beta + j theta beta - theta
beta + phi c + theta c == 0
]
]
]




###########################################
## How to actually be successful!  Here's
## one rewriting the program comes up with.  With the
## variable order heuristic QEPCADB solves this in about
## three seconds! (with ETF solution!)
## What's interesting is that. on the surgace, this looks
## less desireable than the rewriting quoted above.  Hmmmmm...
##########################################

[ ex s[s > 0 /\ mu > 0 /\ beta > 0 /\ sigma > 0 /\ sigma - 1 < 0 /\ c
> 0 /\ phi > 0 /\ theta > 0 
/\ beta s - mu - c < 0 
/\ beta^2 s^2 sigma - beta phi s sigma - beta mu s sigma - beta c s
sigma - beta^2 s sigma + beta mu sigma + beta c sigma - beta s theta +
mu theta + c theta - beta^2 s^2 + beta mu s + 2 beta c s - c mu - c^2
= 0 
/\ beta phi s sigma + beta s theta - mu theta - c theta + beta mu s - mu^2 - c mu > 0]]

[]
(beta,mu,c,sigma,theta,phi,s)
6
(E s)[
s > 0 /\  beta s-mu-c < 0 /\ beta^2 s^2 sigma-beta phi s sigma-beta mu s sigma-beta c s sigma-beta^2 s sigma+beta mu sigma+beta c sigma-beta s theta+mu theta+c theta-beta^2 s^2+beta mu s+2 beta c s-c mu-c^2 = 0 /\ beta phi s sigma+beta s theta-mu theta-c theta+beta mu s-mu^2-c mu > 0].
assume [ mu > 0 /\ beta > 0 /\ sigma > 0 /\ sigma-1 < 0 /\ c > 0 /\ phi > 0 /\ theta > 0 ]
